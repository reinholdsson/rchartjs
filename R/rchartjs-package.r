#' rchartjs
#' 
#' @name rchartjs
#' @docType package
NULL

#' Render Chart.js
#' 
#' Render Chart.js graphs as Shiny output.
#' Wrap a list object (with json options) in server.R.
#' Then use htmlOutput in the ui.R.
#' 
#' @param expr An expression that returns a rchartjs object.
#' @param env The environment in which to evaluate expr.
#' @param quoted If it is a qouted expression.
#' 
#' @export
renderChartjs <- function(expr, env=parent.frame(), quoted=FALSE) {
    func <- shiny::exprToFunction(expr, env, quoted)
    
    function() {
        lst <- func()
        paste(toJSON(lst))
    }
}
